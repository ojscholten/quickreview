# Notes

This file contains general notes made throughout the development of this python
module. Please note that this is not a part of the documentation and may not be
relevant to you.

# AST

Python's standard library [ast](https://docs.python.org/3/library/ast.html)
module provides methods for processing abstract syntax trees (representations
of Python programs). This is very useful in static code analysis tools because
it lets us 'look into' the code without running it.

The module has several useful methods, the main one used here is ast.parse(),
which builds an abstract syntax tree from the contents of a python file. This
tree can then be traversed programmatically, and checked for violations of the
coding conventions described in the module. For example, we can get a list of
all function definitions in the tree by searching the body of the tree for
instances of `FunctionDef`. We can then look through the children of these
instances for things like the name, args, kwargs, and so on.

The arguments object in the ast module contain both args and kwonlyargs
attributes, which themselves are lists of arg objects. These arg objects then
each have an arg attribute, which gives us the name (identifier) of the arg.
For example, lets say we have a function make_pasta(spaghetti, *, tomato).
Having called ast.parse() on the file which contains this function, we would
have a node object. This node object would contain each of the Python objects
in the file, including our `make_pasta` method.

To get the make_pasta method we can do the following;

```python
all_methods = [item for item in node.body
               if item.isinstance(item, ast.FunctionDef)]
make_pasta_method = [method for method in all_methods
                     if method.name == 'make_pasta'][0]




``` 
