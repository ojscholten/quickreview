from quickreview.reviewer import get_python_files


def test_review():

    test_project_files = get_python_files("sample_project/")

    assert len(list(test_project_files)) == 2
