""" Uses the playwright library to read a twitch chat.
No login required.
"""

import time
from playwright.sync_api import sync_playwright


def run(playwright):
    """docstring"""

    firefox = playwright.firefox
    browser = firefox.launch(headless=False)
    page = browser.new_page()
    page.goto("https://www.twitch.tv/popout/loltyler1/chat?popout=")

    message_selector = ".chat-line__message-container"
    page.wait_for_selector(message_selector)
    print("chat found")
    while True:
        messages = page.locator(message_selector).all()
        for message in messages:
            if 'text-fragment' not in message.inner_html():
                continue
            print("="*20)
            print(message.inner_html())
            continue
            try:
                author = message.locator(".chat-author__display-name").inner_text()
                text = message.locator(".text-fragment").inner_text()
                print(author, ":", text)
            except:
                continue
        time.sleep(10)


with sync_playwright() as playwright:
    run(playwright)
