
develop: 
	\
	python -m venv venv; \
	source venv/bin/activate; \
	pip install -r requirements.txt;

clean:
	rm -rf build
	rm -rf venv
	find . -name \*cache\* | xargs rm -rf

# Catch-all target: route all unknown targets to Sphinx using the new
# "make mode" option.  $(O) is meant as a shortcut for $(SPHINXOPTS).
%: Makefile
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

