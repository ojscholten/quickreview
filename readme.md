# quickreview

This tool combines several static code analysis tools like [prospecor]() and
[flake8]() into an easy-to-use and human-friendly code review tool.

To get started using quickreview, install it by calling;

`pip install quickreview`

## Development

To start developing this project, clone the repository and from the root
directory call make develop, this will install all of the dependencies. To call
the library for testing, use;

`python quickreview/cli.py`

## Code Review Opinions

The following set of opinions are highlighted by the quickreview tool;

- all lines should be shorter than 79 characters                         [done]
- methods should have no more than 4 arguments                           [done]
- all methods should use keyword arguments                               [done]
- docstrings should describe all parameters                              [done]
- all methods should have docstrings                                     [done]
- there should be no single letter variable names
- methods should be no longer than 50 lines
- all assertions should have explanatory text
- all parameters and methods should be typed
- there should be no print statements in production code (logger only)
- there should be comments every X lines (statements? 10?)

## Tool Usage Specification

In addition to the opinions listed above, I'm placing several restrictions
on the usage of the tool itself, these restrictions are as follows;

- the tool should output one line per issue found
- it should be easy to use both via CLI and Python
- the tool itself should follow the opinions above
- the output should be human-friendly

