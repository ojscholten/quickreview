"""
Contains functions called by the quickreview CLI.
"""
import pathlib
import ast

from dataclasses import dataclass
from typing import List
from prospector import run as run_prospector
from prospector.config import ProspectorConfig


@dataclass
class Error:
    """ Class for holding violations found by quickreview. """
    file_name: str
    line_number: int
    character: int
    message: str

    def print(self):
        print(self.file_name,
              self.line_number,
              self.character,
              self.message)


def check_line_length(files: List[pathlib.Path],
                      length_cutoff: int = 79) -> List[Error]:
    """ Checks the length of each line in a list of files, returning
    a list of Error's corresponding to each error. """

    message = f"Line has too many characters, maximum={length_cutoff}"
    errors: List[Error] = []
    for file in files:
        with open(file, 'r') as open_file:
            contents = open_file.read()
            for line_num, line in enumerate(contents.split('\n')):
                if len(line) > length_cutoff:
                    error = Error(file, line_num, message)
                    errors.append(error)

    return errors


def check_argument_count(file: pathlib.Path,
                         functions: List[ast.FunctionDef]) -> List[Error]:
    errors = []
    for function in functions:
        kwargs = function.args.kwonlyargs
        args = function.args.args
        line = function.lineno

        # methods should have fewer than 5 arguments
        if len(args) + len(kwargs) > 4:
            error = Error(file, line, 0, "Method has too many arguments ("
                          f"{len(args)+len(kwargs)})")
            errors.append(error)

    return errors


def check_keyword_arguments(file: pathlib.Path,
                            functions: List[ast.FunctionDef]) -> List[Error]:
    errors = []
    for function in functions:
        kwargs = function.args.kwonlyargs
        args = function.args.args
        line = function.lineno
        # there should only be keyword arguments
        if len(kwargs) < len(args) + len(kwargs):
            error = Error(file, line, 0, "Please use keyword "
                          "arguments rather than positional "
                          "arguments.")
            errors.append(error)

    return errors


def check_functions(file: pathlib.Path, node: ast.Module) -> List[Error]:

    functions = [x for x in node.body if isinstance(x, ast.FunctionDef)]

    errors = check_argument_count(file, functions)
    errors += check_keyword_arguments(file, functions)

    for function in functions:
        kwargs = function.args.kwonlyargs
        line = function.lineno
        docstring = ast.get_docstring(function)

        # each keyword argument should be mentioned in the docstring
        for kwarg in kwargs:
            kwarg_name = kwarg.arg
            if docstring is None:
                error = Error(file, line, 0, "No docstring")
                errors.append(error)
                continue
            if docstring.find(kwarg_name):
                error = Error(file, line, 0, "Keyword argument "
                              f"{kwarg_name} isn't mentioned in the "
                              "docstring.")
                errors.append(error)

    return errors


def check_docstrings(file: pathlib.Path) -> List[Error]:
    """ Perform checks on the docstrings of all methods, classes, and functions
    in a python file, returning a list of Error's corresponding to each
    failed check. """

    errors = []
    with open(file, 'r') as open_file:
        node = ast.parse(open_file.read())

        functions = [x for x in node.body
                     if isinstance(x, ast.FunctionDef)]

        print(functions)
    return errors

def check_variable_names(python_files: List[pathlib.Path]):
    
    print("checking variable names...")
    errors = []
    for file in python_files:
        with open(file, 'r') as open_file:
            node = ast.parse(open_file.read())
            names = [x.id for x in ast.walk(node) if isinstance(x, ast.Name)]
            for name in names:
                if len(name) == 1:
                    errors.append(Error(file, 0, 0, name))

    return errors

def run(*, src_dir: pathlib.Path = pathlib.Path(".")) -> None:
    """ Run quickreview's full review on all files in a given directory."""

    all_python_files = src_dir.rglob("*.py")

    for python_file in all_python_files:
        with open(python_file, 'r') as open_file:
            contents = open_file.read()
            node = ast.parse(contents)
            docstring_errors = check_functions(python_file, node)
            for error in docstring_errors:
                error.print()

    line_length_violations = check_line_length(all_python_files)
    for violation in line_length_violations:
        violation.print()

    all_python_files = src_dir.rglob("*.py")
    variable_name_violations = check_variable_names(all_python_files)
    for violation in variable_name_violations:
        violation.print()

    # execute prospector's engine on all files
    prospector_config = ProspectorConfig(src_dir)
    prospector = run_prospector.Prospector(prospector_config)
    prospector.execute()

    if prospector.messages == []:
        print("no errors found, nice!")
    else:
        for message in prospector.messages:
            print(message)


def breaking_method(x, y, *, z, a, b):
    pass


if __name__ == "__main__":
    print("quickreview.review should not be executed as a module, please "
          "call individual methods from this file or use the CLI defined "
          "in quickreview.cli")
