"""
Provides a command line interface for quickreview.
"""
import pathlib
import click

from review import run


@click.command()
@click.argument("src_dir", default=pathlib.Path(','),  type=click.Path(path_type=pathlib.Path))
def run_review(*, src_dir: pathlib.Path) -> None:
    """
    This function creates a 'review', which is a collection of messages
    containing a description of issues in a given file.
    """
    run(src_dir=src_dir)


if __name__ == "__main__":
    run_review()
