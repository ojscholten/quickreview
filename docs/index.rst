.. quickreview documentation master file, created by
   sphinx-quickstart on Sat Mar 25 11:08:56 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to quickreview's documentation!
=======================================

The *quickreview* module aims to help automate the code review process by
providing plain english feedback on Python code.

.. toctree::
   :maxdepth: 2
   :caption: Contents:



